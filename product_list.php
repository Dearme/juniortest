<?php
require_once('database.php');
$stmt = $conn->query('SELECT * FROM PRODUCT');
?>

<html>
<head>
    <meta charset="UTF-8">
    <script src="index.js"></script>
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="product_list.css">

</head>
<body>
<form action="delete_product.php" method="post">
<header>
    <div class="divC">
        <h1 id="headline2">Product List</h1>
    </div>
    <div id="butD" align="right">
        <select id="selectB" class="shadow" name="selected">
            <option value="" selected disabled hidden>---</option>
            <option value="mda">Mass delete actions</option>
        </select>
        <input type="submit" name="button3" value="Apply" class="button1 shadow hower1">
        <input type="button" name="button4" value="Create new" class="button1 shadow" id="create"
               onclick="window.location='add_product.php'">
        <hr class="hr1">
        <br>
        <br>
        <br>
    </div>
</header>

    <div id="parent">
        <?php
        $count=0;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
            <div class="child">
                <input type="checkbox" class="check1" name=<?php echo "_ID[{$count}] " ?> value="<?php echo $row['id'] ?>">
                <p><?php echo $row['sku'] ?></p>
                <p><?php echo $row['name'] ?></p>
                <p><?php echo $row['price'] ?></p>
                <p><?php echo $row['description'] ?></p>
            </div>
        <?php $count++; endwhile; ?>
    </div>
</form>
</body>
</html>