
$(function() {//on document load
    $('#size_div').hide();
    $('#gabarit_div').hide();
    $('#weight_div').hide();
    $('#formType').change(function(){
        showForm($(this).val());
    });

});

function showForm(myFormType){
    switch (myFormType) {
        case "Disc":
            $('#size_div').show();
            $('#gabarit_div').hide();
            $('#weight_div').hide();
            break;
        case "Book":
            $('#gabarit_div').show();
            $('#size_div').hide();
            $('#weight_div').hide();
            break;
        case "Furniture":
            $('#weight_div').show();
            $('#size_div').hide();
            $('#gabarit_div').hide();
            break;
    }
}


