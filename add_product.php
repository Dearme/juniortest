<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="index.js"></script>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="product_add.css">
    </head>
    <body>
        <header>
            <div class="myclass" id="caption1">
                <h1 id="headline">Product Add</h1>
            </div>
            <div id="header" align ="right" class="divC myclass">
                <input type="submit" name="button1" value="Save"  class="button1 shadow" form="add_form">
                <input type="button" name="button2" value="Back to list"  class="button1 shadow"onclick="window.location='product_list.php'">
            </div>
        </header>
        <hr class="hr1">
        <br>
        <br>
        <br>
        <form method="post" action="form_handler.php" id="add_form" name="product_add_form">
            <?phprequire_once('add_product.php');?>
            <div id = "content">
                <label for="enter_sku" class="labelFC"><b>SKU</b></label>
                <input type="text" id="enter_sku" name="sku" class="shadow">
                <br>
                <br>
                <label for="enter_name"class="labelFC"><b>Name</b></label>
                <input type="text" id="enter_name"  name="name" class="shadow">
                <br>
                <br>
                <label for="enter_price" class="labelFC"><b>Price</b></label>
                <input type="text" id="enter_price"  name="price" class="shadow">

                <br>
                <br>
                <br>
                <div>
                    <label><b>Type Switcher</b></label>
                    <select name="formType" id="formType" class="shadow">
                        <option disabled selected>Please select type</option>
                        <option value="Disc">DVD-Disc</option>
                        <option value="Book">Book</option>
                        <option value="Furniture">Furniture</option>
                    </select>
                    <br>
                    <br>

                    <div class="div1" id="size_div">
                        <label for="text4" class="labelFC2"><b>Size</b></label>
                        <input type="text" name="size" id="text4" class="shadow">
                    </div>


                    <div class="div1" id="gabarit_div">
                        <label for="enter_height" class="labelFC2"><b>Height</b></label>
                        <input type="text" name="height" id="enter_height" class="shadow">
                        <br>
                        <br>
                        <label for="enter_width" class="labelFC2"><b>Width</b></label>
                        <input type="text" name="width" id="enter_width" class="shadow">
                        <br>
                        <br>
                        <label for="enter_lenght" class="labelFC2"><b>Lenght</b></label>
                        <input type="text" name="lenght" id="enter_lenght" class="shadow">
                    </div>


                    <div class="div1" id="weight_div">
                        <label for="enter_weight" class="labelFC2"><b>Weight</b></label>
                        <input type="text" name="weight" id="enter_weight" class="shadow">
                    </div>
                    </form>
                </body>
            </html>